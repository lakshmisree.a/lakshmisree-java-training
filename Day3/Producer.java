import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    private BlockingQueue<String> queue;

    public Producer(BlockingQueue<String> q){
        this.queue=q;
    }
    @Override
    public void run() {
        System.out.println("Start enqueuing by producer");
        //produce message
        for(int i=0; i<100; i++){
            try {
                Thread.sleep(i);
                queue.put(i+"");
                System.out.println("Successful enqueuing byProduced "+i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            queue.put("exit");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}