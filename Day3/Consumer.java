import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{

    private BlockingQueue<String> queue;

    public Consumer(BlockingQueue<String> q){
        this.queue=q;
    }

    @Override
    public void run() {
        try{
            String msg;
            //consuming messages until exit message is received
            while((msg = queue.take()) !="exit"){
                System.out.println("Successful de-queuing/consumption by Consumer "+msg);
            }
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}
