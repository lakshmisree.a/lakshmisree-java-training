package com.company;

import java.util.LinkedList;
import java.util.List;

public class Program3 {
    public static void main(String args[]) {
        List<String> ll1 = new LinkedList<String>();
        ll1.add("Newyork");
        ll1.add("Ohio");
        ll1.add("Kansas");

        List<String> ll2 = new LinkedList<String>();
        //Copying LinkedList1 into LinkedList2
        ll2.addAll(ll1);
        //Printing linked list 2
        System.out.println("LinkedList 2 is - " +ll2);

    }
}
