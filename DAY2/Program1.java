package com.company;

import java.util.LinkedList;
import java.util.List;

public class Program1 {

    public static void main(String args[]) {
        List<String> ll1 = new LinkedList<String>();
        ll1.add("Newyork");
        ll1.add("Ohio");
        ll1.add("Kansas");
        ll1.add("Texas");
        ll1.add("Maryland");

        System.out.println("Before updating : " +ll1);
        //Updating an element
        ll1.set(2, "Chicago");

        //Updated List
        System.out.println("After updating : " +ll1);

    }

}
