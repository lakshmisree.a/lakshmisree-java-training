package com.company;

import java.util.TreeSet;

public class Program8 {

    public static void main(String[] argv)
    {
        TreeSet<String>
                set1 = new TreeSet<String>();

        //set1
        set1.add("Calhoun");
        set1.add("Bishop");
        set1.add("Lowell");
        set1.add("Ludlow");
        set1.add("Jefferson");

        System.out.println("First TreeSet: "
                + set1);

        TreeSet<String>
                set2 = new TreeSet<String>();

        // set2
        set2.add("Lowell");
        set2.add("Morrison");
        set2.add("Ludlow");

        System.out.println("Second TreeSet: "
                + set2);

        for(String s:set1){
            System.out.println(set2.contains(s) ? "YES": "NO");
        }

    }
}
