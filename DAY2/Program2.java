package com.company;

import java.util.LinkedList;
import java.util.List;

public class Program2 {

    public static void main(String args[]) {
        List<String> ll1 = new LinkedList<String>();
        ll1.add("Newyork");
        ll1.add("Ohio");
        ll1.add("Kansas");
        ll1.add("Texas");
        ll1.add("Maryland");

        //searching for an element in Array List
        System.out.println("Is the element present in list - " + ll1.contains("Ohio"));
    }
}
