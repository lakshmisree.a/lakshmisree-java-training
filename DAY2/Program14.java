package com.company;
import java.util.*;

class Employee {

    private String name;
    private int ID;

    public Employee(String name, Integer ID)
    {
        this.name = name;
        this.ID = ID;
    }

    public String getName() { return this.name; }

    public Integer getID() { return this.ID; }
    // override toString method
    public String toString()
    {
        return this.name + ": " + ID;
    }
}

// Comparator that sort elements according to ID's in
// Ascending order
class ComparingID implements Comparator<Employee> {
    public int compare(Employee e1, Employee e2)
    {
        return e1.getID().compareTo(e2.getID());
    }
}


public class Program14 {

        public static void main(String[] args)
        {

            // New TreeMap 
            TreeMap<Employee, Integer> map
                    = new TreeMap<Employee, Integer>(new ComparingID());

            // Adding elements to TreeMap
            map.put(new Employee("Lakshmi", 17898765), 1);
            map.put(new Employee("Sree", 67854321), 2);
            map.put(new Employee("Addala", 9874321), 3);

            System.out.println(
                    "TreeMap keys sorting in Accending order of the ID:");
            // Print map
            for (Map.Entry<Employee, Integer> entry :
                    map.entrySet()) {
                System.out.println("Key : (" + entry.getKey()
                        + "), Value : "
                        + entry.getValue());
            }
        }
    }

