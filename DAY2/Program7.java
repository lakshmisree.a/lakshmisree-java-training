package com.company;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Program7 {

    public static void main(String[] args)
    {

        // Get the HashSet
        Set<String> hashset = new HashSet<>();
        hashset.add("Calcutta");
        hashset.add("Hyderabad");
        hashset.add("Chennai");
        hashset.add("Bangalore");
        hashset.add("Mumbai");

        System.out.println("HashSet: "
                + hashset);

        // Convert the HashSet to TreeSet
        Set<String> hashSetToTreeSet
                = new TreeSet<>(hashset);

        // Print the TreeSet
        System.out.println("TreeSet: "
                + hashSetToTreeSet);
    }
}
