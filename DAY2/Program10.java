package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Program10 {

    public static void main(String args[])
    {
        PriorityQueue<String> pqe = new PriorityQueue<String>();
        pqe.add("Alaska");
        pqe.add("Mexico");
        pqe.add("Ohio");
        pqe.add("Cincinnati");
        pqe.add("Texas");
        System.out.println(pqe);
        String[] arr = new String[pqe.size()];
        pqe.toArray(arr);

        System.out.println("After Converting to Array");
        for( String i: arr )
        {
            System.out.println(i);
        }
    }

}
