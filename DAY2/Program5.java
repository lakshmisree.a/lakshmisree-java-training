package com.company;

import java.util.LinkedList;
import java.util.List;

public class Program5 {

    public static void main(String[] args)
    {
        List<String> ll = new LinkedList<String>();
        ll.add("Newyork");
        ll.add("Ohio");
        ll.add("Kansas");

        //adding element at a specific index
        ll.add(2,"Chicago");
        ll.add(3,"Carolina");

        //Printing LinkedList
        System.out.println("Elements in Linked List");
        for(String i:ll)
        {
            System.out.println(i);
        }

        //Verifying Indexes
        System.out.println("Index of Chicago is - " +ll.indexOf("Chicago"));
        System.out.println("Index of Carolina is -" +ll.indexOf("Carolina"));
    }
}
