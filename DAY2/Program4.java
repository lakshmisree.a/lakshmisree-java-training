package com.company;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Program4 {

    public static void main(String args[])
    {
        List<String> ll = new LinkedList<String>();
        ll.add("Newyork");
        ll.add("Ohio");
        ll.add("Kansas");

        //Iterating in Reverse Direction
        //Could even use descending Iterator - ll.descendingIterator()
        ListIterator itr = ll.listIterator(ll.size());
        while(itr.hasPrevious())
        {
            System.out.println(itr.previous());
        }

    }
}
