package com.company;

import java.util.HashMap;

public class Program11 {

    public static void main(String[] args)
    {

        // Creating an empty HashMap
        HashMap<String, Integer> hash_map = new HashMap<String, Integer>();

        // Mapping int values to string keys
        hash_map.put("Alaska", 10);
        hash_map.put("Ohio", 15);
        hash_map.put("Pittsburgh", 20);
        hash_map.put("Texas", 25);
        hash_map.put("Cincinnati", 30);

        // Original HashMap
        System.out.println("The Mappings are: " + hash_map);

        // Do the HashMap have Key-value mappings?
        System.out.println("Is the map empty? " + hash_map.isEmpty());

        // Checking for the key_element 'World'
        System.out.println("Is the key 'Java' present? " +
                hash_map.containsKey("Java"));
    }
}
