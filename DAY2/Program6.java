package com.company;

import java.util.Collections;
import java.util.LinkedList;

public class Program6 {

    public static void main(String[] args)
    {
        LinkedList<String> linkedList = new LinkedList <String> ();

        linkedList.add("Houston");
        linkedList.add("Richardson");
        linkedList.add("Texas");
        linkedList.add("Plano");
        linkedList.add("Galvington");

        System.out.println("Linked list before shuffling:\n" + linkedList);
        //Shuffling the LinkedList elements
        Collections.shuffle(linkedList);
        System.out.println("Linked list after shuffling:\n" + linkedList);

    }
}
