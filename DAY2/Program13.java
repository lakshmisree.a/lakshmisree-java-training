package com.company;

import java.util.*;
import java.util.Map.Entry;
public class Program13 {
    public static void main(String args[]) {

        // Create a tree map
        TreeMap <String,String> tree_map1 = new TreeMap <String,String> ();

        // Put elements to the map
        tree_map1.put("Alaska", "Red");
        tree_map1.put("Ohio", "Green");
        tree_map1.put("Seattle", "Black");
        tree_map1.put("Pittsburgh", "White");

        System.out.println("Orginal TreeMap content: " + tree_map1);
        System.out.println("Reverse order view of the keys: " + tree_map1.descendingKeySet());
    }
}
